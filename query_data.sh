# id: apple
# 
curl "http://localhost:8983/solr/gettingstarted/select?q=id:apple"
curl "http://192.168.1.58:8983/solr/gettingstarted/select?q=search_text:*"


# pry -r rsolr
solr = RSolr.connect :url => 'http://localhost:8983/solr/gettingstarted/'

# search
    response = solr.get 'select', :params => {:q => 'id:apple'}
  # or
    solr.select params: {q: 'id:1'}
    re = solr.select params: {q: 'city_id:*'}

    re['response']['docs'].count
    re['response']['numFound']

# paginate
  # solr.paginate #{start}, #{rows}
    solr.paginate 1, 10, "select", :params => {:q => "city_id:*"}


# add with hash
  doc = {:id=>2, :job_id => 'jobid_1', :job_title => 'some title'}
  solr.add [doc]
  solr.commit

# add one data only
  solr.add :id=>1, :address_s => 'random address'
  solr.commit
  solr.get 'select', :params => {:q => 'id:1'}


# delete
  solr.delete_by_id 1
  solr.delete_by_query 'id:apple'
