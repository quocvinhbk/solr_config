require_relative './helpers'

require 'mysql2'
require 'rsolr'
require 'rsolr-ext'
require 'pry'

@client = Mysql2::Client.new(host: 'localhost', username: 'root')

# @city_table = @client.query("SELECT * FROM  venjobs_app_development.cities")
# @industry_table = @client.query("SELECT * FROM venjobs_app_development.industries")

def update_solr
  solr = RSolr.connect url: 'http://localhost:8983/solr/gettingstarted/'

  # # city_table = @client.query('SELECT * FROM  venjobs_app_development.cities')
  # # industry_table = @client.query('SELECT * FROM venjobs_app_development.industries')
  # # jobs_table = @client.query('SELECT * FROM venjobs_app_development.industries_jobs WHERE job_id = 1235')

  # # update Jobs table
  # job_table = @client.query('SELECT * FROM  venjobs_app_development.jobs')

  # if job_table.any?
  #   job_table.each_with_index do |job,id|
  #     break if id == 3
  #     # check is job_id is exist and not update job to solr
  #     response_solr = solr.get 'select', params: { q: "job_id: #{job['id']}" }
  #     next if response_solr['response']['docs'].any?
      
  #     # hash of job attribute array: city_name, ... 
  #     hash_table = {}
      
  #     ind_table1 = @client.query("SELECT * FROM venjobs_app_development.industries_jobs WHERE job_id = #{job['id']}")
  #     ind_id_arr =  if ind_table1.any?
  #                     ind_table1.collect { |ind| ind['industry_id']}
  #                   end
      
  #     ind_id_arr.each do |ind_id|
  #       x = @client.query('SELECT * FROM venjobs_app_development.industries')
  #       ind_arr[ind_id.to_s] = x.first
  #       binding.pry
  #     end
      
  #     # if job_id is not exit: add job to solr
  #     # job_city = city_table.select { |city| city['id'] == job['city_id'] }.first
  #     # job_industry = industry_table.select { |ind| ind['id'] == job['industry_id'] }.first
      
  #     # jobs_table = @client.query("SELECT * FROM venjobs_app_development.industries_jobs WHERE job_id = #{job['id']}")

  #     # doc = { job_id:             job['id'],
  #     #         job_title:          job['title'].downcase,
  #     #         job_city_id:        job_city['id'],
  #     #         job_city_name:      job_city['name'].downcase,
  #     #         job_city_slug:      job_city['slug'],
  #     #         job_industry_id:    job_industry['id'],
  #     #         job_industry_name:  job_industry['name'].downcase,
  #     #         job_industry_slug:  job_industry['slug'] 
  #     #       }

  #     # solr.add [doc]
  #   end

  #   # solr.commit
  # end
  solr.delete_by_query '*:*'
  solr.commit
end

print_memory_usage do
  print_time_spent do
    update_solr
    # retrive_solr
  end
end
