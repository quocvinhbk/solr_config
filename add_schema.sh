#!/bin/bash
# # Document
# # Add field to schema
# curl -X POST -H 'Content-type:application/json' --data-binary '{
#   "add-field":{
#      "name":"job_name",
#      "default":""
#      "type":"string",
#      "stored":true }
# }' http://localhost:8983/solr/gettingstarted/schema

# # Delete field
# curl -X POST -H 'Content-type:application/json' --data-binary '{
#   "delete-field" : { "name":"job_name" }
# }' http://localhost:8983/solr/gettingstarted/schema

# # Add a copy field 
# curl -X POST -H 'Content-type:application/json' --data-binary '{
#   "add-copy-field":{
#      "source":"shelf",
#      "dest":[ "location", "catchall" ]}
# }' http://localhost:8983/solr/gettingstarted/schema

# retrive schema
# curl http://localhost:8983/solr/gettingstarted/schema/fields

# # Add job_id
# curl -X POST -H 'Content-type:application/json' --data-binary '{
#   "add-field":{
#      "name":"job_id",
#      "type":"string",
#      "indexed": true,
#      "stored":true }
# }' http://localhost:8983/solr/gettingstarted/schema

# # Add job_title
# curl -X POST -H 'Content-type:application/json' --data-binary '{
#   "add-field":{
#      "name":"job_title",
#      "type":"string",
#      "indexed": true,
#      "stored":true }
# }' http://localhost:8983/solr/gettingstarted/schema

# # Add city_id 
# curl -X POST -H 'Content-type:application/json' --data-binary '{
#   "add-field":{
#      "name":"city_id",
#      "type":"string",
#      "indexed": true,
#      "stored":true }
# }' http://localhost:8983/solr/gettingstarted/schema

# # Add city_name
# curl -X POST -H 'Content-type:application/json' --data-binary '{
#   "add-field":{
#      "name":"city_name",
#      "type":"string",
#      "indexed": true,
#      "stored":true }
# }' http://localhost:8983/solr/gettingstarted/schema


# # Add city_slug
# curl -X POST -H 'Content-type:application/json' --data-binary '{
#   "add-field":{
#      "name":"city_slug",
#      "type":"string",
#      "indexed": true,
#      "stored":true }
# }' http://localhost:8983/solr/gettingstarted/schema


# # Add search_text
# curl -X POST -H 'Content-type:application/json' --data-binary '{
#   "add-field":{
#      "name":"search_text",
#      "type":"string",
#      "indexed": true,
#      "stored":false,
#      "multiValued": true }
# }' http://localhost:8983/solr/gettingstarted/schema


# # Delete field
# curl -X POST -H 'Content-type:application/json' --data-binary '{
#   "delete-field" : { "name":"search_text" }
# }' http://localhost:8983/solr/gettingstarted/schema

# # Add copy field
# curl -X POST -H 'Content-type:application/json' --data-binary '{
#   "add-copy-field":{
#      "source":"city_slug",
#      "dest":"search_text" }
# }' http://localhost:8983/solr/gettingstarted/schema


# ## DELETE FIELD

# # Delete copy field
# curl -X POST -H 'Content-type:application/json' --data-binary '{
#   "delete-copy-field":{ "source":"job_id", "dest":"search_text" }
# }' http://localhost:8983/solr/gettingstarted/schema

# # Delete copy field
# curl -X POST -H 'Content-type:application/json' --data-binary '{
#   "delete-copy-field":{ "source":"job_title", "dest":"search_text" }
# }' http://localhost:8983/solr/gettingstarted/schema

# # Delete copy field
# curl -X POST -H 'Content-type:application/json' --data-binary '{
#   "delete-copy-field":{ "source":"job_city_id", "dest":"search_text" }
# }' http://localhost:8983/solr/gettingstarted/schema

# # Delete copy field
# curl -X POST -H 'Content-type:application/json' --data-binary '{
#   "delete-copy-field":{ "source":"job_city_name", "dest":"search_text" }
# }' http://localhost:8983/solr/gettingstarted/schema

# # Delete copy field
# curl -X POST -H 'Content-type:application/json' --data-binary '{
#   "delete-copy-field":{ "source":"job_city_slug", "dest":"search_text" }
# }' http://localhost:8983/solr/gettingstarted/schema


# # Delete copy field
# curl -X POST -H 'Content-type:application/json' --data-binary '{
#   "delete-copy-field":{ "source":"job_industry_id", "dest":"search_text" }
# }' http://localhost:8983/solr/gettingstarted/schema

# # Delete copy field
# curl -X POST -H 'Content-type:application/json' --data-binary '{
#   "delete-copy-field":{ "source":"job_industry_name", "dest":"search_text" }
# }' http://localhost:8983/solr/gettingstarted/schema

# # Delete copy field
# curl -X POST -H 'Content-type:application/json' --data-binary '{
#   "delete-copy-field":{ "source":"job_industry_slug", "dest":"search_text" }
# }' http://localhost:8983/solr/gettingstarted/schema



# # Delete field
# curl -X POST -H 'Content-type:application/json' --data-binary '{
#   "delete-field" : { "name":"search_text" }
# }' http://localhost:8983/solr/gettingstarted/schema

# # Delete field
# curl -X POST -H 'Content-type:application/json' --data-binary '{
#   "delete-field" : { "name":"job_id" }
# }' http://localhost:8983/solr/gettingstarted/schema

# # Delete field
# curl -X POST -H 'Content-type:application/json' --data-binary '{
#   "delete-field" : { "name":"job_title" }
# }' http://localhost:8983/solr/gettingstarted/schema

# # Delete field
# curl -X POST -H 'Content-type:application/json' --data-binary '{
#   "delete-field" : { "name":"job_city_id" }
# }' http://localhost:8983/solr/gettingstarted/schema

# # Delete field
# curl -X POST -H 'Content-type:application/json' --data-binary '{
#   "delete-field" : { "name":"job_city_name" }
# }' http://localhost:8983/solr/gettingstarted/schema

# # Delete field
# curl -X POST -H 'Content-type:application/json' --data-binary '{
#   "delete-field" : { "name":"job_city_slug" }
# }' http://localhost:8983/solr/gettingstarted/schema


# # Delete field
# curl -X POST -H 'Content-type:application/json' --data-binary '{
#   "delete-field" : { "name":"job_industry_id" }
# }' http://localhost:8983/solr/gettingstarted/schema

# # Delete field
# curl -X POST -H 'Content-type:application/json' --data-binary '{
#   "delete-field" : { "name":"job_industry_name" }
# }' http://localhost:8983/solr/gettingstarted/schema

# # Delete field
# curl -X POST -H 'Content-type:application/json' --data-binary '{
#   "delete-field" : { "name":"job_industry_slug" }
# }' http://localhost:8983/solr/gettingstarted/schema


# ############ ADD NEW SCHEMA #################
# # Add job_id
# curl -X POST -H 'Content-type:application/json' --data-binary '{
#   "add-field":{
#      "name":"job_id",
#      "type":"string",
#      "indexed": true,
#      "stored":true }
# }' http://localhost:8983/solr/gettingstarted/schema

# # Add job_title
# curl -X POST -H 'Content-type:application/json' --data-binary '{
#   "add-field":{
#      "name":"job_title",
#      "type":"string",
#      "indexed": true,
#      "stored":true }
# }' http://localhost:8983/solr/gettingstarted/schema


# # Add job_city_id
# curl -X POST -H 'Content-type:application/json' --data-binary '{
#   "add-field":{
#      "name":"job_city_id",
#      "type":"string",
#      "indexed": true,
#      "stored":true,
#      "multiValued": true  }
# }' http://localhost:8983/solr/gettingstarted/schema

# # Add job_city_name
# curl -X POST -H 'Content-type:application/json' --data-binary '{
#   "add-field":{
#      "name":"job_city_name",
#      "type":"string",
#      "indexed": true,
#      "stored":true 
#      "multiValued": true  }
# }' http://localhost:8983/solr/gettingstarted/schema

# # Add job_city_slug
# curl -X POST -H 'Content-type:application/json' --data-binary '{
#   "add-field":{
#      "name":"job_city_slug",
#      "type":"string",
#      "indexed": true,
#      "stored":true 
#      "multiValued": true  }
# }' http://localhost:8983/solr/gettingstarted/schema


# # Add job_industry_id
# curl -X POST -H 'Content-type:application/json' --data-binary '{
#   "add-field":{
#      "name":"job_industry_id",
#      "type":"string",
#      "indexed": true,
#      "stored":true,
#      "multiValued": true }
# }' http://localhost:8983/solr/gettingstarted/schema

# # Add job_industry_name
# curl -X POST -H 'Content-type:application/json' --data-binary '{
#   "add-field":{
#      "name":"job_industry_name",
#      "type":"string",
#      "indexed": true,
#      "stored":true,
#      "multiValued": true }
# }' http://localhost:8983/solr/gettingstarted/schema

# # Add job_industry_slug
# curl -X POST -H 'Content-type:application/json' --data-binary '{
#   "add-field":{
#      "name":"job_industry_slug",
#      "type":"string",
#      "indexed": true,
#      "stored":true,
#      "multiValued": true }
# }' http://localhost:8983/solr/gettingstarted/schema


# # Add company_name
# curl -X POST -H 'Content-type:application/json' --data-binary '{
#   "add-field":{
#      "name":"job_company_name",
#      "type":"string",
#      "indexed": true,
#      "stored":true }
# }' http://localhost:8983/solr/gettingstarted/schema


# # Add search_text
# curl -X POST -H 'Content-type:application/json' --data-binary '{
#   "add-field":{
#      "name":"search_text",
#      "type":"text_en",
#      "indexed": true,
#      "stored":false,
#      "multiValued": true }
# }' http://localhost:8983/solr/gettingstarted/schema

# # Add copy field job_id
# curl -X POST -H 'Content-type:application/json' --data-binary '{
#   "add-copy-field":{
#      "source":"job_id",
#      "dest":"search_text" }
# }' http://localhost:8983/solr/gettingstarted/schema

# # Add copy field job_title
# curl -X POST -H 'Content-type:application/json' --data-binary '{
#   "add-copy-field":{
#      "source":"job_title",
#      "dest":"search_text" }
# }' http://localhost:8983/solr/gettingstarted/schema

# # Add copy field job_city_id
# curl -X POST -H 'Content-type:application/json' --data-binary '{
#   "add-copy-field":{
#      "source":"job_city_id",
#      "dest":"search_text" }
# }' http://localhost:8983/solr/gettingstarted/schema


# # Add copy field job_city_name
# curl -X POST -H 'Content-type:application/json' --data-binary '{
#   "add-copy-field":{
#      "source":"job_city_name",
#      "dest":"search_text" }
# }' http://localhost:8983/solr/gettingstarted/schema


# # Add copy field job_city_slug
# curl -X POST -H 'Content-type:application/json' --data-binary '{
#   "add-copy-field":{
#      "source":"job_city_slug",
#      "dest":"search_text" }
# }' http://localhost:8983/solr/gettingstarted/schema

# # Add copy field job_industry_id
# curl -X POST -H 'Content-type:application/json' --data-binary '{
#   "add-copy-field":{
#      "source":"job_industry_id",
#      "dest":"search_text" }
# }' http://localhost:8983/solr/gettingstarted/schema


# # Add copy field job_industry_name
# curl -X POST -H 'Content-type:application/json' --data-binary '{
#   "add-copy-field":{
#      "source":"job_industry_name",
#      "dest":"search_text" }
# }' http://localhost:8983/solr/gettingstarted/schema


# # Add copy field job_industry_slug
# curl -X POST -H 'Content-type:application/json' --data-binary '{
#   "add-copy-field":{
#      "source":"job_industry_slug",
#      "dest":"search_text" }
# }' http://localhost:8983/solr/gettingstarted/schema

# # Add copy field job_industry_slug
# curl -X POST -H 'Content-type:application/json' --data-binary '{
#   "add-copy-field":{
#      "source":"job_company_name",
#      "dest":"search_text" }
# }' http://localhost:8983/solr/gettingstarted/schema

# ############ DONE ADD SCHEMA #################
